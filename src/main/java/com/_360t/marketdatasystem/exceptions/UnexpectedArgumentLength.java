package com._360t.marketdatasystem.exceptions;

/***
 * This exception is thrown when argument is wrong size.
 */
public class UnexpectedArgumentLength extends Exception {

	// Serialization
	private static final long serialVersionUID = -2131903470026856749L;

	/***
	 * Constructor.
	 * 
	 * @param given
	 *            - number of values passed
	 * @param expected
	 *            - number of values expected
	 */
	public UnexpectedArgumentLength(int given, int expected) {
		super("Invalid argument size. Given: " + given + "; Expected: " + expected);
	}
}
