package com._360t.marketdatasystem.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import com._360t.marketdatasystem.util.ObjectSize;

/**
 * This class establishes basic connection to a remote destination. It is also
 * able to track traffic exchanged.
 */
public abstract class Client implements TrafficUsage {

	// Connection parameters
	private URL url;
	private URLConnection urlConnection;

	// Traffic data exchanged
	private long uploadTraffic = 0;
	private long downloadTraffic = 0;

	/**
	 * Constructor.
	 * 
	 * @param url
	 *            - remote address
	 */
	public Client(URL url) {
		this.url = url;
	}

	/**
	 * Upload data to output stream. This abstract method should be overridden
	 * in subclasses.
	 * 
	 * @param data
	 *            - object to be written to output stream
	 * @throws IOException
	 */
	protected abstract void upload(Object data) throws IOException;

	/**
	 * Read data from input stream. This abstract method should be overridden in
	 * subclasses.
	 * 
	 * @return data read
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	protected abstract Object read() throws IOException, ClassNotFoundException;

	/**
	 * Send data to remote server and track data-size sent.
	 * 
	 * @param data
	 * @throws IOException
	 */
	public final void send(Object data) throws IOException {
		upload(data);
		addTrafficSent(ObjectSize.getByteCount(data));
	}

	/**
	 * Read object sent from remote device and track data-size received.
	 * 
	 * @return object read
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public final Object receive() throws IOException, ClassNotFoundException {
		Object received = read();
		addTrafficReceived(ObjectSize.getByteCount(received));
		return received;
	}

	/**
	 * Get {@code URLConnection} instance of the URL object.
	 * 
	 * @return URLConnection instance
	 * @throws IOException
	 *             error while opening connection
	 */
	protected URLConnection getUrlConnection() throws IOException {
		if (urlConnection == null)
			urlConnection = (URLConnection) url.openConnection();

		return urlConnection;
	}

	/**
	 * Get output stream of the connection.
	 * 
	 * @return output stream
	 * @throws IOException
	 *             error while getting output stream
	 */
	public OutputStream getOutputStream() throws IOException {
		 return urlConnection.getOutputStream();
	}

	/**
	 * Get input stream of the connection.
	 * 
	 * @return input stream
	 * @throws IOException
	 *             error while getting input stream
	 */
	public InputStream getInputStream() throws IOException {
		return urlConnection.getInputStream();
	}

	@Override
	public void addTrafficSent(long uploadTraffic) {
		this.uploadTraffic += uploadTraffic;
	}

	@Override
	public void addTrafficReceived(long downloadTraffic) {
		this.downloadTraffic += downloadTraffic;
	}

	@Override
	public long getSentTraffic() {
		return uploadTraffic;
	}

	@Override
	public long getReceivedTraffic() {
		return downloadTraffic;
	}

	/**
	 * Close output stream related to this connection.
	 */
	public void closeOutput() {
		OutputStream out;
		try {
			out = getOutputStream();
			if (out != null)
				out.close();

		} catch (IOException e) {
			System.err.println("Exception while closing OutputStream: " + e.getMessage());
		}
	}

	/**
	 * Close input stream.
	 */
	public void closeInput() {
		InputStream in;
		try {
			in = getInputStream();
			if (in != null)
				in.close();
		} catch (IOException e) {
			System.err.println("Exception while closing OutputStream: " + e.getMessage());
		}
	}
}
