package com._360t.marketdatasystem.client;

/**
 * 
 * This interface should be implemented by classes which need to measure traffic
 * sent and traffic received.
 */
public interface TrafficUsage {

	/**
	 * Add traffic amount to the overall traffic sent.
	 * 
	 * @param uploadTraffic
	 *            -
	 */
	void addTrafficSent(long uploadTraffic);

	/**
	 * Add traffic amount to the overall traffic received.
	 * 
	 * @param downloadTraffic
	 */
	void addTrafficReceived(long downloadTraffic);

	/**
	 * Get sent traffic amount.
	 * 
	 * @return all traffic sent
	 */
	long getSentTraffic();

	/**
	 * Get received traffic amount.
	 * 
	 * @return all traffic received
	 */
	long getReceivedTraffic();
}
