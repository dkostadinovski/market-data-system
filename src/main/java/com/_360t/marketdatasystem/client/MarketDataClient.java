package com._360t.marketdatasystem.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;

import com._360t.marketdatasystem.transferable.Transferable;
import com._360t.marketdatasystem.transferable.Transferable.TransferPeriod;
import com._360t.marketdatasystem.transferable.marketdata.MarketData;
import com._360t.marketdatasystem.transferable.marketdata.MarketDataBasic;
import com._360t.marketdatasystem.transferable.marketdata.MarketDataOptimized;
import com._360t.marketdatasystem.util.DataCommiter;
import com._360t.marketdatasystem.util.StatisticsCommiter;

/**
 * This class is used for sending {@code Transferable} objects and receive
 * statistical info about data sent.
 *
 * @see MarketData
 * @see MarketDataBasic
 * @see MarketDataOptimized
 */
public class MarketDataClient extends HttpClient {

	// Object streams
	private ObjectOutputStream objectOutputStream;
	private ObjectInputStream objectInputStream;

	/**
	 * This constructor should be used when using HTTP connection without SSL.
	 * 
	 * @param httpUrl
	 *            - the URL address of remote server
	 * @throws IOException
	 */
	public MarketDataClient(URL httpUrl) throws IOException {
		super(httpUrl);
	}

	/**
	 * This constructor should be only used when connecting through HTTPS (HTTP
	 * with SSL).
	 * 
	 * @param httpsUrl
	 *            - the URL address of remote server
	 * @param keystorePath
	 *            - path where the keystore is stored
	 * @param keystorePass
	 *            - password for keystore file
	 * @throws KeyManagementException
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 */
	public MarketDataClient(URL httpsUrl, String keystorePath, String keystorePass) throws KeyManagementException,
			KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		super(httpsUrl, keystorePath, keystorePass);
	}

	@Override
	protected void upload(Object object) throws IOException {
		getObjectOutputStream().writeObject(object);
	}

	@Override
	protected Object read() throws IOException, ClassNotFoundException {
		return getObjectInputStream().readObject();
	}

	/**
	 * Commit/Save values using {@link DataCommiter} interface.
	 * 
	 * @param dataCommiter
	 *            - object implementing {@code DataCommiter}
	 * @param values
	 *            - values to be commit/saved
	 * @throws Exception
	 * @see {@link StatisticsCommiter}
	 */
	public void commitValues(DataCommiter dataCommiter, Object[] values) throws Exception {
		dataCommiter.commitValues(values);
	}

	/**
	 * Read {@code Transferable[]} data send from the server as a response to
	 * this client's HTTP request.
	 * 
	 * @return array of {@code Transferable} objects sent from server
	 * @throws Exception
	 */
	public Transferable[] readResponseFully() throws Exception {

		ArrayList<Transferable> receivedData = new ArrayList<Transferable>();
		boolean read = true;
		while (read) {
			try {
				Transferable tmpData = (Transferable) receive();
				tmpData.setTransferTime(TransferPeriod.END);
				receivedData.add(tmpData);
			} catch (IOException e) {
				read = false;
			}
		}

		return receivedData.toArray(new Transferable[receivedData.size()]);
	}

	/**
	 * Get {@code ObjectOutputStream} associated to this client's connection
	 * with the server.
	 * 
	 * @return ObjectOutputStream of the connection between this client and
	 *         remote server
	 * @throws IOException
	 */
	private ObjectOutputStream getObjectOutputStream() throws IOException {
		if (objectOutputStream == null) {
			objectOutputStream = new ObjectOutputStream(getOutputStream());
		}

		return objectOutputStream;
	}

	/**
	 * Get {@code ObjectInputStream} associated to this client's connection with
	 * the server.
	 * 
	 * @return ObjectInputStream of the connection between this client and
	 *         remote server
	 * @throws IOException
	 */
	private ObjectInputStream getObjectInputStream() throws IOException {
		if (objectInputStream == null)
			objectInputStream = new ObjectInputStream(getInputStream());

		return objectInputStream;
	}
}