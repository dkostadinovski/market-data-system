package com._360t.marketdatasystem.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

/**
 * This class provides HTTP and HTTPS connection.
 *
 */
public abstract class HttpClient extends Client {

	// HTTP/HTTPS connections
	private HttpsURLConnection httpsUrlConnection;
	private HttpURLConnection httpUrlConnection;

	// True if the client should use HTTPS connection
	private boolean useHttps;

	/**
	 * Use this constructor only when connecting with HTTP.
	 * 
	 * @param url
	 *            - url of the remote destination (url string should contain
	 *            {@code http://})
	 * @throws IOException
	 *             error while connecting to remote destination
	 */
	public HttpClient(URL url) throws IOException {
		super(url);

		this.useHttps = false;
		httpUrlConnection = (HttpURLConnection) getUrlConnection();

		// Set http connection parameters
		httpUrlConnection.setDoOutput(true);
		httpUrlConnection.setDoInput(true);
	}

	/**
	 * Use this constructor only when connecting through HTTPS.
	 * 
	 * @param url
	 *            - remote address (url string should contain {@code https://})
	 * @param keystorePath
	 *            - filepath of the server's certificate
	 * @param keystorePass
	 *            - password for the server's certificate
	 * @throws IOException
	 *             error while making https connection
	 * @throws KeyStoreException
	 *             error while accessing keystore certificate
	 * @throws NoSuchAlgorithmException
	 *             error while defining security algorithm
	 * @throws CertificateException
	 * @throws KeyManagementException
	 */
	public HttpClient(URL url, String keystorePath, String keystorePass) throws IOException, KeyStoreException,
			NoSuchAlgorithmException, CertificateException, KeyManagementException {

		super(url);
		this.useHttps = true;

		httpsUrlConnection = (HttpsURLConnection) getUrlConnection();
		httpsUrlConnection.setDoOutput(true);
		httpsUrlConnection.setDoInput(true);

		// Set SSL parameters
		KeyStore ksCACert = KeyStore.getInstance(KeyStore.getDefaultType());
		ksCACert.load(new FileInputStream(keystorePath), keystorePass.toCharArray());
		TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
		tmf.init(ksCACert);
		SSLContext context = SSLContext.getInstance("TLS");
		context.init(null, tmf.getTrustManagers(), null);
		SSLSocketFactory sslSocketFactory = context.getSocketFactory();
		httpsUrlConnection.setSSLSocketFactory(sslSocketFactory);
	}

	/**
	 * Set chunked streaming mode. This is important because the client will
	 * send data as soon as it has {@code chunkLen} data available in its
	 * OutputStream.
	 * 
	 * @param chunkLen
	 *            - the chunks length
	 */
	public void setChunkedStreamingMode(int chunkLen) {
		if (useHttps)
			httpsUrlConnection.setChunkedStreamingMode(chunkLen);
		else
			httpUrlConnection.setChunkedStreamingMode(chunkLen);
	}

	/**
	 * This method actually makes the connection to the remote destination.
	 * 
	 * @throws IOException
	 */
	public void connect() throws IOException {
		if (useHttps)
			httpsUrlConnection.connect();
		else
			httpUrlConnection.connect();
	}

	@Override
	protected abstract void upload(Object data) throws IOException;

	@Override
	protected abstract Object read() throws IOException, ClassNotFoundException;
}
