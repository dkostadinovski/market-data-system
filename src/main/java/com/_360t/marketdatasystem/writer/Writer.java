package com._360t.marketdatasystem.writer;

/***
 * This interface should be implemented by classes performing writing of String
 * values.
 * 
 */
public interface Writer {

	/***
	 * Write String values into specified object.
	 * 
	 * @param values
	 *            - String values to be written
	 * @throws Exception
	 *             - if something goes wrong
	 */
	void writeValues(String[] values) throws Exception;
}
