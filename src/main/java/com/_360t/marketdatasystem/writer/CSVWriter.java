package com._360t.marketdatasystem.writer;

import java.io.FileWriter;
import java.io.IOException;

import com._360t.marketdatasystem.exceptions.UnexpectedArgumentLength;

/***
 * This class provides CSV file writer. It writes string values separated by
 * {@value delimiter} into CSV prefixed file.
 * 
 * <p>
 * Variable {@code numOfValues} represents the number of elements (columns)
 * separated by the delimiter.
 * </p>
 * 
 * @see Writer
 * @see FileWriter
 */
public class CSVWriter extends FileWriter implements Writer {

	// Variables
	private static final String DEFAULT_DELIMITER = ",";
	private String delimiter = DEFAULT_DELIMITER;

	/***
	 * Constructor.
	 * 
	 * @param fileName
	 *            - The filename of CSV file.
	 * @param append
	 *            - Indicates if the CSV file should be appendable.
	 * @param numOfValues
	 *            - Number of values separated by the delimiter.
	 * @throws IOException
	 */
	public CSVWriter(String fileName, boolean append) throws IOException {
		super(fileName, append);
	}

	/**
	 * Set delimiter separating values.
	 * 
	 * @param delimiter
	 *            - separate values
	 */
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	/***
	 * Get delimiter separating values.
	 * 
	 * @return {@code delimiter} representation separating values
	 */
	public String getDelimiter() {
		return delimiter;
	}

	/***
	 * Use this method to add columns' descriptions.
	 * 
	 * @param values
	 *            - columns' descriptions
	 * @throws IOException
	 *             exception while writing values
	 * @throws UnexpectedArgumentLength
	 *             if wrong number of values is given
	 */
	public void writeHeaderValues(String[] values) throws IOException, UnexpectedArgumentLength {
		writeValues(values);
	}

	public void writeValues(String[] values) throws IOException, UnexpectedArgumentLength {
		// Number of values
		int valuesLen = values.length;

		// Create number builder
		StringBuilder lineBuilder = new StringBuilder();
		for (int i = 0; i < values.length; i++) {
			if (values[i] != null)
				lineBuilder.append(values[i]);
			else
				lineBuilder.append("-");

			if (i != valuesLen - 1)
				lineBuilder.append(delimiter);
		}

		// Append New Line
		lineBuilder.append(System.lineSeparator());

		// Write values-separated line into file
		super.write(lineBuilder.toString());
		super.flush();
	}
}
