package com._360t.marketdatasystem;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.HashMap;
import java.util.Random;

import com._360t.marketdatasystem.client.MarketDataClient;
import com._360t.marketdatasystem.transferable.Transferable;
import com._360t.marketdatasystem.transferable.Transferable.TransferPeriod;
import com._360t.marketdatasystem.transferable.marketdata.Mapper;
import com._360t.marketdatasystem.transferable.marketdata.MarketData;
import com._360t.marketdatasystem.transferable.marketdata.MarketDataBasic;
import com._360t.marketdatasystem.transferable.marketdata.MarketDataOptimized;
import com._360t.marketdatasystem.util.DataCommiter;
import com._360t.marketdatasystem.util.PropertiesReader;
import com._360t.marketdatasystem.util.ParameterScanner;
import com._360t.marketdatasystem.util.StatisticsCommiter;
import com._360t.marketdatasystem.writer.CSVWriter;

/**
 * This class provides simulation of sending data objects between client and
 * server. It should be used only for testing purposes.
 * 
 * <p>
 * Simulation flow:<br>
 * 
 * Stage 1:<br>
 * - Read parameters from configuration file. File-path of the configuration
 * file should be passed as first argument in main method.<br>
 * - Read parameters from standard input.<br>
 * <br>
 * 
 * Stage 2:<br>
 * - Establish HTTP or HTTPS connection to the remote server. <br>
 * - Create and send {@link MarketData} objects to the remote destination. Data
 * for {@code MarketData} objects is chosen randomly from parameters given in
 * configuration file.<br>
 * <br>
 * 
 * Stage 3:<br>
 * - Read statistics data from remote destination.<br>
 * <br>
 * 
 * Stage 4:<br>
 * - Save statistics data in CSV file.<br>
 * <br>
 * 
 * Stage 5:<br>
 * - Show statistics about traffic exchange.<br>
 * </p>
 */
public class Simulator {
	// Standard-input scanner
	private static ParameterScanner stdin = new ParameterScanner(System.in);

	/**
	 * Main method.
	 * 
	 * @param args
	 *            - first parameter should be the filepath of properties file
	 */
	public static void main(String[] args) {

		// Get properties file path
		String propertiesPath = "config.properties";
		if (args.length > 0)
			propertiesPath = args[0];

		System.out.println("Simulator started");
		try {
			int index = 1;
			while (true) {
				System.out.println("Perform new simulation? yes - no ");
				String ready = stdin.readString(new String[] { "yes", "no" });

				// Perform Simulation?
				if (ready.equalsIgnoreCase("yes")) {
					System.out.println("SIMULATION " + index);
					System.out.println("Reading properties...");
					HashMap<String, Object> paramteres = getSimulationParameters(propertiesPath);

					// Start simulation
					performSimulation(paramteres);
					index++;
				} else
					break;
			}

			System.out.print("You can exit now");
			stdin.readString(null);
		} catch (Exception e) {
			System.err.println("Exception occured: " + e.getMessage());
		} finally {
			stdin.closeScanner();
		}
	}

	/**
	 * Perform simulation.
	 * 
	 * @param parameters
	 *            - needed for running this simulation
	 * @throws Exception
	 */
	private static void performSimulation(HashMap<String, Object> parameters) throws Exception {

		/*******************
		 * Get parameters *
		 *******************/
		String protocol = (String) parameters.get("protocol");
		String host = (String) parameters.get("host");
		int port = (Integer) parameters.get("port");
		int portHttps = (Integer) parameters.get("portHttps");
		String keystorePath = (String) parameters.get("keystorePath");
		String keystorePass = (String) parameters.get("keystorePass");
		int chunkLen = (Integer) parameters.get("chunkLen");
		int objNum = (Integer) parameters.get("objNum");
		String objectType = (String) parameters.get("objectType");
		String[] banks = (String[]) parameters.get("banks");
		String[] symbols = (String[]) parameters.get("symbols");
		String[] prices = (String[]) parameters.get("prices");
		String statsFilename = (String) parameters.get("statsFilename");
		String statsDelimiter = (String) parameters.get("statsDelimiter");

		// Create mappings between banks and symbols
		Mapper mapper = new Mapper(banks, symbols);

		/************************
		 * Establish connection *
		 ************************/
		URL url = null;
		MarketDataClient client = null;
		System.out.println("Connecting to server...");
		if (protocol.equalsIgnoreCase("http")) {
			url = new URL(protocol + "://" + host + ":" + port);
			client = new MarketDataClient(url);
		} else {
			url = new URL(protocol + "://" + host + ":" + portHttps);
			client = new MarketDataClient(url, keystorePath, keystorePass);
		}
		client.setChunkedStreamingMode(chunkLen);
		client.connect();

		/**************************************************************
		 * Create MarketData objects and upload objects to the Server
		 **************************************************************/
		System.out.println("Sending data...");

		// Send Mapper object first,
		// if we are sending MarketDataOptimized objects
		if (objectType.equalsIgnoreCase("optimized"))
			client.send(mapper);

		for (int i = 1; i <= objNum; i++) {
			String symbol = symbols[getRandom(0, symbols.length - 1)];
			String bank = banks[getRandom(0, banks.length - 1)];
			BigDecimal price = new BigDecimal(prices[getRandom(0, prices.length - 1)]);
			MarketData tmp = null;
			if (objectType.equalsIgnoreCase("basic"))
				tmp = new MarketDataBasic(i, symbol, bank, price);
			else
				tmp = new MarketDataOptimized(i, mapper.getIdentifier(bank, symbol), price);
			tmp.setTransferTime(TransferPeriod.START);
			client.send(tmp);
		}
		// Close output stream
		client.closeOutput();
		System.out.println("Total: " + objNum + " objects sent.\n");

		/**************************************
		 * Reading response data from server.
		 **************************************/
		System.out.println("Reading response data...");
		Transferable[] statistics = client.readResponseFully();
		System.out.println("Total " + statistics.length + " objects read.\n");

		/**********************
		 * Writing Statistics *
		 **********************/
		System.out.println("Writing statistics into file " + statsFilename);
		CSVWriter writer = new CSVWriter(statsFilename, false);
		writer.setDelimiter(statsDelimiter);
		writer.writeHeaderValues(new String[] { "ID", "RTT", "MD-Up-Start", "MD-Up-End", "Stat-Down-Start", "Stat-Down-End" });
		DataCommiter statisticsCommiter = new StatisticsCommiter(writer);
		client.commitValues(statisticsCommiter, statistics);
		client.closeInput();
		writer.close();

		/**************************
		 * Show traffic statistics*
		 **************************/
		System.out.println();
		System.out.println("Upload traffic: " + client.getSentTraffic() + " Bytes");
		System.out.println("Download traffic: " + client.getReceivedTraffic() + " Bytes");
		System.out.println("=========================================================\n");
	}

	/**
	 * Return parameters required for current simulation.
	 * 
	 * @param propertiesPath
	 * @return parameters for this simulation
	 * @throws IOException
	 */
	private static HashMap<String, Object> getSimulationParameters(String propertiesPath) throws IOException {
		HashMap<String, Object> propFile = readPropertiesFile(propertiesPath);
		HashMap<String, Object> propStdin = readStdinProperties();

		HashMap<String, Object> result = new HashMap<String, Object>();
		result.putAll(propFile);
		result.putAll(propStdin);

		return result;
	}

	/**
	 * Read parameters from Standard Input
	 * 
	 * @return parameters read from stdin
	 */
	private static HashMap<String, Object> readStdinProperties() {
		HashMap<String, Object> parameters = new HashMap<String, Object>();

		System.out.print("Number of objects to be sent? ");
		parameters.put("objNum", stdin.readInteger());

		System.out.print("Markt Data Objects? basic - optimized ");
		parameters.put("objectType", stdin.readString(new String[] { "basic", "optimized" }));

		System.out.print("Protocol? http - https ");
		parameters.put("protocol", stdin.readString(new String[] { "http", "https" }));

		System.out.println("Enter filename for statistics file");
		String workingDirectory = System.getProperty("user.dir");
		System.out.print("\tCurrent directory: " + workingDirectory + "\n");
		String statsFilename = stdin.readString(null);
		if (!statsFilename.endsWith(".csv"))
			statsFilename += ".csv";
		statsFilename = workingDirectory + File.separator + statsFilename;
		parameters.put("statsFilename", statsFilename);

		return parameters;
	}

	/**
	 * Return parameters read from configuration file.
	 * 
	 * @param propertiesPath
	 * @return parameters read from configuration file
	 * @throws IOException
	 */
	private static HashMap<String, Object> readPropertiesFile(String propertiesPath) throws IOException {
		PropertiesReader properties = new PropertiesReader(propertiesPath);
		HashMap<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("host", properties.readProperty("host", "localhost"));
		parameters.put("port", properties.readProperty("port", 80));
		parameters.put("portHttps", properties.readProperty("port-https", 8443));
		parameters.put("chunkLen", properties.readProperty("chunk-len", 1024));
		parameters.put("keystorePath", properties.readProperty("keystore-path", "../src/main/resources/keystore.jks"));
		parameters.put("keystorePass", properties.readProperty("keystore-pass", "marketdata"));
		parameters.put("statsDelimiter", properties.readProperty("stat-delimiter", ","));

		// Market Data simulation
		parameters.put("symbols", properties.readProperty("symbols", new String[] { "EUR", "USD" }));
		parameters.put("banks", properties.readProperty("banks", new String[] { "Commerzbank", "GLS Bank" }));
		parameters.put("prices", properties.readProperty("prices", new String[] { "1.2345", "67.89" }));

		// Return parameters read from properties file
		return parameters;
	}

	/**
	 * Return random integer between range min-max.
	 * 
	 * @param min
	 *            - range min value
	 * @param max
	 *            - range max value
	 * @return random integer
	 */
	private static int getRandom(int min, int max) {
		return new Random().nextInt((max - min) + 1) + min;
	}
}
