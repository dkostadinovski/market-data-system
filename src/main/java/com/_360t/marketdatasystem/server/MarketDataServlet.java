package com._360t.marketdatasystem.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com._360t.marketdatasystem.server.handler.MarketDataHandler;

/**
 * This servlet should be used for handling requests transferring
 * {@link MarketData} data.
 * 
 */
public class MarketDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("New POST request from: " + req.getRemoteHost());
		MarketDataHandler handler = new MarketDataHandler(req, resp);
		handler.handle();
	}
}
