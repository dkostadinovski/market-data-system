package com._360t.marketdatasystem.server.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com._360t.marketdatasystem.transferable.Transferable;
import com._360t.marketdatasystem.transferable.TransferableProcessor;

/**
 * This Request Handler should be used for processing {@link MarketData} objects
 * sent from client.
 * 
 * <p>
 * This handler reads the stream of MarketData objects sent from client. Then
 * process them and makes statistical objects about received data. At the end
 * sends statistical objects back to client.
 * </p>
 *
 */
public class MarketDataHandler extends TransferableHandler {

	/**
	 * Constructor.
	 * 
	 * @param request - client's request for sending MarketData instances
	 * @param response - servlet's response to the client
	 * @throws IOException
	 */
	public MarketDataHandler(HttpServletRequest request, HttpServletResponse response) throws IOException {
		super(request, response);
	}

	@Override
	public void handle() throws IOException {
		try {
			// Read all transferable data on the Input stream
			System.out.println("Reading request data...");
			Transferable[] receivedData = readTransferableFully();
			System.out.println("Total objects read: " + receivedData.length);

			// Process data
			System.out.println("Processing data...");
			Transferable[] processedData = null;
			if (receivedData != null && receivedData.length > 0) {
				// Get processor
				TransferableProcessor processor = TransferableProcessorFactory.getProcessor(receivedData[0]);
				// Process received data
				processedData = processor.processTransferable(receivedData);
			}

			// Send
			System.out.println("Sending response data...");
			sendTransferable(processedData);
			System.out.println("Total objects sent: " + processedData.length);

		} catch (ClassNotFoundException e) {
			System.out.println("Exception while casting object from InputStream: " + e.getMessage());
			sendError(500, "Exception at server: " + e.getMessage());
		} finally {
			closeInput();
			closeOutput();
			System.out.println();
		}
	}
}
