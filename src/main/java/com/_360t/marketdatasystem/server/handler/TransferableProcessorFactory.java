package com._360t.marketdatasystem.server.handler;

import com._360t.marketdatasystem.transferable.Transferable;
import com._360t.marketdatasystem.transferable.TransferableProcessor;
import com._360t.marketdatasystem.transferable.marketdata.Mapper;
import com._360t.marketdatasystem.transferable.marketdata.MarketData;

/**
 * 
 * This class is used as Object Factory, returning the appropriate
 * transferable-data-processor for specific stream-data.
 */
public class TransferableProcessorFactory {

	/**
	 * Get processor for {@code Transferable} data array. The first object of
	 * transferable array data determines the type of processor required.
	 * 
	 * @param firstObj
	 *            - first {@code Transferable} object of the array
	 * @return transferable processor
	 */
	public static TransferableProcessor getProcessor(Transferable firstObj) {
		if (firstObj instanceof MarketData)
			return new MarketDataStatProcessor();
		else if (firstObj instanceof Mapper)
			return new MarketDataOptiStatProcessor();

		return null;
	}
}
