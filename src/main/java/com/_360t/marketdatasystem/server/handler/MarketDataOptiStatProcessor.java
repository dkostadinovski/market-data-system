package com._360t.marketdatasystem.server.handler;

import java.util.Arrays;

import com._360t.marketdatasystem.transferable.Transferable;
import com._360t.marketdatasystem.transferable.marketdata.Mapper;

/**
 * This data-processor should be used for processing {@code MarketDataOptimized}
 * objects. It creates statistics about {@code MarketDataOptimized} passed to
 * {@code processTransferable(Transferable[] transArr)}.
 * 
 * <p>
 * This statistics-processor is same as {@link MarketDataStatProcessor}, except
 * it detects if {@link Mapper} objects is also present.
 * </p>
 *
 * @see Mapper
 */
public class MarketDataOptiStatProcessor extends MarketDataStatProcessor {

	@SuppressWarnings("unused")
	private Mapper mapper;

	@Override
	public Transferable[] processTransferable(Transferable[] transArr) {
		// Check if the first object is of type Mapper
		if (transArr != null && transArr[0] instanceof Mapper) {
			this.mapper = (Mapper) transArr[0];
			transArr = Arrays.copyOfRange(transArr, 1, transArr.length);
		}

		// Mapper is used to get Bank and Symbol values from
		// MarketDataOptimized object.

		return super.processTransferable(transArr);
	}
}
