package com._360t.marketdatasystem.server.handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * HTTP is request-response protocol. Response's output stream should be opened
 * only after reading the request. After opening output stream, reading from
 * request's input stream is disabled
 */
public abstract class DefaultHandler {

	// Request and response
	protected HttpServletRequest request;
	protected HttpServletResponse response;

	/**
	 * Constructor.
	 * 
	 * @param request
	 * @param response
	 */
	public DefaultHandler(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	/**
	 * Handle request received.
	 * 
	 * @throws IOException
	 */
	abstract void handle() throws IOException;

	/**
	 * Send generic HTTP error.
	 * 
	 * @param errCode
	 *            - error code
	 * @param errDesc
	 *            - error's description
	 * @throws IOException
	 */
	public final void sendError(int errCode, String errDesc) throws IOException {
		response.sendError(errCode, errDesc);
	}

	public final void sendTextResponse(String response) throws IOException {
		getOutputStream().write(response.getBytes());
	}

	/**
	 * Get input stream of the request.
	 * 
	 * @return request's input stream
	 * @throws IOException
	 */
	protected InputStream getInputStream() throws IOException {
		return request.getInputStream();
	}

	/**
	 * Get output stream of the response.
	 * 
	 * @return response's output stream
	 * @throws IOException
	 */
	protected OutputStream getOutputStream() throws IOException {
		return response.getOutputStream();
	}

	/**
	 * Close input stream.
	 */
	public void closeInput() {
		InputStream in;
		try {
			in = getInputStream();
			if (in != null)
				in.close();
		} catch (IOException e) {
			System.err.println("Exception while closing OutputStream: " + e.getMessage());
		}
	}

	/**
	 * Close output stream related to this connection.
	 */
	public void closeOutput() {
		OutputStream out;
		try {
			out = getOutputStream();
			if (out != null)
				out.close();

		} catch (IOException e) {
			System.err.println("Exception while closing OutputStream: " + e.getMessage());
		}
	}
}
