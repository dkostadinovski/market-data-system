package com._360t.marketdatasystem.server.handler;

import com._360t.marketdatasystem.transferable.Transferable;
import com._360t.marketdatasystem.transferable.TransferableProcessor;
import com._360t.marketdatasystem.transferable.marketdata.MarketData;
import com._360t.marketdatasystem.transferable.marketdata.MarketDataStatistic;

/**
 * This data-processor should be used for processing {@code MarketData} objects.
 * It creates statistics about {@code MarketData} passed to
 * {@code processTransferable(Transferable[] transArr)}.
 *
 */
public class MarketDataStatProcessor implements TransferableProcessor {

	@Override
	public Transferable[] processTransferable(Transferable[] transArr) {
		if (transArr == null)
			return null;

		int dataLen = transArr.length;
		MarketDataStatistic[] statistics = new MarketDataStatistic[dataLen];
		for (int i = 0; i < dataLen; i++) {
			MarketData mData = (MarketData) transArr[i];
			MarketDataStatistic temp = new MarketDataStatistic(mData.getID(), mData.getStartTransferTime(),
					mData.getEndTransferTime());
			statistics[i] = temp;
		}

		return statistics;
	}
}
