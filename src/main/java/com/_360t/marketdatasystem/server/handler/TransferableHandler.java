package com._360t.marketdatasystem.server.handler;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com._360t.marketdatasystem.transferable.Transferable;
import com._360t.marketdatasystem.transferable.Transferable.TransferPeriod;

/**
 * This handler should be inherited by classes handling data sub-classed from
 * {@code Transferable}.
 *
 */
public abstract class TransferableHandler extends DefaultHandler {

	// Input and Output
	private ObjectOutputStream objectOutputStream;
	private ObjectInputStream objectInputStream;

	/**
	 * Constructor.
	 * 
	 * @param request
	 * @param response
	 */
	public TransferableHandler(HttpServletRequest request, HttpServletResponse response) {
		super(request, response);
	}

	@Override
	abstract void handle() throws IOException;

	/**
	 * Send {@code Transferable} object to remote destination.
	 * 
	 * @param transObject
	 *            - object to be sent
	 * @throws IOException
	 */
	protected void sendTransferable(Transferable transObject) throws IOException {
		transObject.setTransferTime(TransferPeriod.START);
		getObjectOutputStream().writeObject(transObject);
	}

	/**
	 * Read one {@code Transferable} object from input stream.
	 * 
	 * @return {@code Transferable} object read
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	protected Transferable readTransferable() throws ClassNotFoundException, IOException {
		Transferable transferable = (Transferable) getObjectInputStream().readObject();
		if (transferable != null)
			transferable.setTransferTime(TransferPeriod.END);
		return transferable;
	}

	/**
	 * Send array of {@code Transferable} objects.
	 * 
	 * @param transArr
	 *            - array of objects to be sent
	 * @throws IOException
	 */
	protected void sendTransferable(Transferable[] transArr) throws IOException {
		if (transArr == null)
			return;

		for (Transferable transObject : transArr)
			sendTransferable(transObject);
	}

	/**
	 * Read {@code Transferable} data until client closes its output stream.
	 * 
	 * @return data received from client
	 * @throws ClassNotFoundException
	 */
	protected Transferable[] readTransferableFully() throws ClassNotFoundException {
		ArrayList<Transferable> receivedData = new ArrayList<Transferable>();
		boolean read = true;

		while (read)
			try {
				receivedData.add(readTransferable());
			} catch (IOException e) {
				read = false;
			}

		return receivedData.toArray(new Transferable[receivedData.size()]);
	}

	/**
	 * Get ObjectOutputStream of this connection.
	 * 
	 * @return object output stream
	 * @throws IOException
	 */
	private ObjectOutputStream getObjectOutputStream() throws IOException {
		if (objectOutputStream == null)
			objectOutputStream = new ObjectOutputStream(getOutputStream());

		return objectOutputStream;
	}

	/**
	 * Get ObjectInputStream of this connection.
	 * 
	 * @return object input stream
	 * @throws IOException
	 */
	private ObjectInputStream getObjectInputStream() throws IOException {
		if (objectInputStream == null)
			objectInputStream = new ObjectInputStream(getInputStream());

		return objectInputStream;
	}
}
