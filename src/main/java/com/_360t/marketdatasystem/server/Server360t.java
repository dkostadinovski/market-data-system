package com._360t.marketdatasystem.server;

import java.io.IOException;

import javax.servlet.http.HttpServlet;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.server.ssl.SslSelectChannelConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.ssl.SslContextFactory;

import com._360t.marketdatasystem.util.PropertiesReader;

/***
 * Embedded Jetty server.
 * 
 */
public class Server360t {

	// Parameters
	private Server server;
	private ServletContextHandler context;
	private String keyStorePath;
	private String keyStorePass;

	/***
	 * Server is initialized to run on given port.
	 * 
	 * @param httpPort
	 */
	public Server360t() {
		server = new Server();
		keyStorePath = "";
		keyStorePass = "";
	}

	/**
	 * Initialize server's connectors and servlets.
	 * 
	 * @param httpPort
	 * @param httpsPort
	 */
	public void init(int httpPort, int httpsPort) {
		// Set HTTP connector
		SelectChannelConnector httpConnector = new SelectChannelConnector();
		httpConnector.setPort(httpPort);

		// Set HTTPS connector
		SslSelectChannelConnector ssl_connector = new SslSelectChannelConnector();
		ssl_connector.setPort(httpsPort);
		SslContextFactory cf = ssl_connector.getSslContextFactory();
		cf.setKeyStorePath(keyStorePath);
		cf.setKeyStorePassword(keyStorePass);

		context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");
		server.setHandler(context);
		server.setConnectors(new Connector[] { httpConnector, ssl_connector });
	}

	/***
	 * Add specific servlet for handling HTTP requests. ,
	 * 
	 * @param servlet
	 *            which will handle requests.
	 * @param path
	 *            at where the servlet is reachable.
	 */
	public void addServlet(HttpServlet servlet, String path) {
		context.addServlet(new ServletHolder(servlet), path);
	}

	/***
	 * Start server.
	 * 
	 * @throws Exception
	 *             indicating something went wrong.
	 */
	public void start() throws Exception {
		server.start();
		server.join();
	}

	/***
	 * Main method.
	 * 
	 * <p>
	 * This method provides starting of Jetty-embedded server. The first
	 * argument should be the file-path of configuration file.
	 * </p>
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			String propertiesPath = "config.properties";
			if (args.length > 0)
				propertiesPath = args[0];

			// Read parameters
			PropertiesReader properties = new PropertiesReader(propertiesPath);
			int httpPort = properties.readProperty("port", 80);
			int httpsPort = properties.readProperty("port-https", 443);
			String keyPath = properties.readProperty("keystore-path", "../src/main/resources/keystore.jks");
			String keyPass = properties.readProperty("keystore-pass", "marketdata");

			Server360t server360t = new Server360t();
			server360t.keyStorePath = keyPath;
			server360t.keyStorePass = keyPass;

			// Initialize connectors
			server360t.init(httpPort, httpsPort);

			// Add servlet handlers
			server360t.addServlet(new MarketDataServlet(), "/*");

			// Start server
			server360t.start();
		} catch (IOException ioExcep) {
			System.err.println("Server cannot be started.\nReason: " + ioExcep.getMessage());
		} catch (Exception generalExcep) {
			System.err.println("Server cannot be started.\nReason: " + generalExcep.getMessage());
		}
	}
}
