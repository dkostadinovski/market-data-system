package com._360t.marketdatasystem.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/***
 * This class calculates object's size.
 * 
 */
public class ObjectSize {

	/***
	 * Return object's size in bytes.
	 * 
	 * @param object
	 *            - size of this object is calculated
	 * @return object's size in bytes
	 * @throws IOException
	 *             exception occurred while calculating object's size
	 */
	public static long getByteCount(Object object) throws IOException {
		ByteArrayOutputStream byteArrOS = new ByteArrayOutputStream();
		ObjectOutputStream objectOS = new ObjectOutputStream(byteArrOS);
		objectOS.writeObject(object);
		return byteArrOS.toByteArray().length;
	}
}
