package com._360t.marketdatasystem.util;

/**
 * This interface should be implemented by classes which purpose is to
 * commit/save values.
 * 
 */
public interface DataCommiter {

	/**
	 * Commit/Save {@code Object} values.
	 * 
	 * @param values
	 *            - data to be committed
	 * @throws Exception
	 *             error while committing values
	 */
	void commitValues(Object[] values) throws Exception;
}
