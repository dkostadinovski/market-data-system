package com._360t.marketdatasystem.util;

import com._360t.marketdatasystem.transferable.marketdata.MarketDataStatistic;
import com._360t.marketdatasystem.writer.Writer;

/**
 * This class should be used only for writing statistics of {@code MarketData}
 * objects. It writes info about {@code MarketData} object's ID, time needed to
 * reach the remote destination and time needed to download statistical info. It
 * also calculates round-trip-time (upload + download time).
 */
public class StatisticsCommiter implements DataCommiter {
	// Writer of statistical data
	private Writer writer;

	// Constructor
	public StatisticsCommiter(Writer writer) {
		this.writer = writer;
	}

	/**
	 * Committed values: <br>
	 * - ID of Market Data <br>
	 * - Upload time of Market Data <br>
	 * - Download time of Market Data Statistics <br>
	 * - Round-trip time
	 */
	@Override
	public void commitValues(Object[] values) throws Exception {
		for (Object obj : values) {
			MarketDataStatistic stat = (MarketDataStatistic) obj;

			// Upload/Download time
			long mDataUploadStart = stat.getUploadStart();
			long mDataUploadEnd = stat.getUploadEnd();
			long statDownloadStart = stat.getDownloadStart();
			long statDownloadEnd = stat.getDownloadEnd();

			int id = stat.getID();
			long roundTripTime = (mDataUploadEnd - mDataUploadStart) + (statDownloadEnd - statDownloadStart);

			writer.writeValues(new String[] { 
					String.valueOf(id),
					String.valueOf(roundTripTime),
					String.valueOf(mDataUploadStart), 
					String.valueOf(mDataUploadEnd), 
					String.valueOf(statDownloadStart),
					String.valueOf(statDownloadEnd) });
		}
	}
}