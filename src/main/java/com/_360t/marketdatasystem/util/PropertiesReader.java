package com._360t.marketdatasystem.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This class is used to read properties values from a properties file.
 */
public class PropertiesReader {

	// Parameters
	private Properties properties;
	private InputStream inputStream;

	/**
	 * Constructor.
	 * <p>
	 * Properties file should be read from project's resources.
	 * </p>
	 * 
	 * @param propertiesPath
	 *            - the filepath of properties file
	 * @throws IOException
	 *             error while reading values
	 */
	public PropertiesReader(String propertiesPath) throws IOException {
		properties = new Properties();

		inputStream = getClass().getClassLoader().getResourceAsStream(propertiesPath);

		if (inputStream != null) {
			properties.load(inputStream);
		} else {
			throw new FileNotFoundException("property file '" + propertiesPath + "' not found in the classpath");
		}
	}

	/**
	 * Read string property.
	 * 
	 * @param propertyName
	 *            - property to be read
	 * @param defaultValue
	 *            - if property not found, return default value
	 * @return property value
	 */
	public String readProperty(String propertyName, String defaultValue) {
		return properties.getProperty(propertyName, defaultValue);
	}

	/**
	 * Read integer property.
	 * 
	 * @param propertyName
	 *            - property to be read
	 * @param defaultValue
	 *            - if property not found, return default value
	 * @return property value
	 */
	public int readProperty(String propertyName, int defaultValue) {
		String property = readProperty(propertyName, "" + defaultValue);
		if (property != null) {
			return Integer.parseInt(property);
		}

		return defaultValue;
	}

	/**
	 * Read boolean property.
	 * 
	 * @param propertyName
	 *            - property to be read
	 * @param defaultValue
	 *            - if property not found, return default value
	 * @return property value
	 */
	public boolean readProperty(String propertyName, boolean defaultValue) {
		String property = readProperty(propertyName, "" + defaultValue);
		if (property != null) {
			return Boolean.parseBoolean(property);
		}

		return defaultValue;
	}

	/**
	 * Read string-array property.
	 * 
	 * @param propertyName
	 *            - property to be read
	 * @param defaultValue
	 *            - if property not found, return default value
	 * @return property value
	 */
	public String[] readProperty(String propertyName, String[] defaultValue) {
		String[] values = ((String) properties.get(propertyName)).split(",");
		if (values != null) {
			for (int i = 0; i < values.length; i++)
				values[i] = values[i].trim();

			return values;
		}

		return defaultValue;
	}
}
