package com._360t.marketdatasystem.util;

import java.io.InputStream;
import java.util.Scanner;

/**
 * 
 * This class simplifies the scanning of String, Integer and Boolean data types.
 */
public class ParameterScanner {
	private Scanner scanner;

	/**
	 * Constructor.
	 * 
	 * @param input
	 *            - input stream to be scanned
	 */
	public ParameterScanner(InputStream input) {
		scanner = new Scanner(input);
	}

	/**
	 * Read string value.
	 * 
	 * <p>
	 * If {@code acceptableValues} is not null, then scanner scans until value
	 * from {@code acceptableValues} is provided. </br>
	 * If {@code acceptableValues} is null, then any value provided is returned
	 * as valid String value.
	 * </p>
	 * 
	 * @param acceptableValues
	 *            - possible values that can be inserted
	 * @return string value provided
	 */
	public String readString(String[] acceptableValues) {
		String input = "";
		while (true) {
			input = scanner.next();
			if (acceptableValues == null)
				return input;

			for (String val : acceptableValues)
				if (val.equalsIgnoreCase(input))
					return input;

			System.out.print("Invalid input. Possible values: ");
			for (String str : acceptableValues)
				System.out.print(str + "; ");
			System.out.println();
		}
	}

	/**
	 * Read boolean value.
	 * 
	 * @return boolean provided
	 */
	public boolean readBoolean() {
		boolean input = false;
		while (true)
			try {
				input = scanner.nextBoolean();
				break;
			} catch (Exception e) {
				System.out.print("Invalid input. Possible values: true; false; ");
				scanner.next();
			}

		return input;
	}

	/**
	 * Return integer value.
	 * 
	 * @return int value provided
	 */
	public int readInteger() {
		int input = 0;
		while (true)
			try {
				input = scanner.nextInt();
				break;
			} catch (Exception e) {
				System.out.print("Invalid input. Enter only integer values! ");
				scanner.next();
			}

		return input;
	}

	/**
	 * Close scanner.
	 */
	public void closeScanner() {
		try {
			scanner.close();
		} catch (Exception e) {
			System.out.println("Exception while closing Scanner");
		}
	}
}
