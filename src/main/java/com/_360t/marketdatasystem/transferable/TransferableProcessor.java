package com._360t.marketdatasystem.transferable;

/**
 * This interface should be implemented by classes which are able to process
 * {@link Transferable} data.
 * 
 */
public interface TransferableProcessor {

	/**
	 * Process {@code Transferable} data array.
	 * 
	 * @param transArr
	 *            - data to be processed
	 * @return processed {@code Transferable} data
	 */
	Transferable[] processTransferable(Transferable[] transArr);
}
