package com._360t.marketdatasystem.transferable.marketdata;

/**
 * This class is used for storing statistical data about market-data objects.
 *
 * @see MarketData
 * @see MarketDataBasic
 * @see MarketDataOptimized
 */
public class MarketDataStatistic extends MarketData {
	private static final long serialVersionUID = 1L;

	// Time instances of uploading MarketData object
	private long uploadStart;
	private long uploadEnd;

	/**
	 * Constructor.
	 * 
	 * @param ID
	 *            - identification of uploaded market data
	 * @param uploadStart
	 *            - the start time of uploading {@link MarketData} object
	 * @param uploadEnd
	 *            - the end time of uploading {@link MarketData} object
	 */
	public MarketDataStatistic(int ID, long uploadStart, long uploadEnd) {
		super(ID);
		this.uploadStart = uploadStart;
		this.uploadEnd = uploadEnd;
	}

	/**
	 * Get the upload start time of {@link MarketData} object.
	 * 
	 * @return upload start time
	 */
	public long getUploadStart() {
		return uploadStart;
	}

	/**
	 * Get the upload end time of {@link MarketData} object.
	 * 
	 * @return upload end time
	 */
	public long getUploadEnd() {
		return uploadEnd;
	}

	/**
	 * Get the download start time of {@link MarketDataStatistic} object.
	 * 
	 * @return download start time
	 */
	public long getDownloadStart() {
		return super.getStartTransferTime();
	}

	/**
	 * Get the download end time of {@link MarketDataStatistic} object.
	 * 
	 * @return download end time
	 */
	public long getDownloadEnd() {
		return super.getEndTransferTime();
	}
}
