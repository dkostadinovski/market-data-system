package com._360t.marketdatasystem.transferable.marketdata;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import com._360t.marketdatasystem.transferable.Transferable;

/**
 * This class is used to map byte value to its correspondent Bank and Symbol
 * value.
 *
 * <p>
 * The object of this class should be sent first in the stream of
 * {@link MarketDataOptimized} objects. It will provide information about banks
 * and symbols of the {@code MarketData} objects at remote's location.
 * </p>
 * 
 * @see MarketDataOptimized
 */
@SuppressWarnings("serial")
public class Mapper extends Transferable {

	// Mapping between byte value and banks-symbol values
	private HashMap<Byte, String[]> mappings;

	/**
	 * Constructor.
	 * 
	 * <p>
	 * Create mapping between byte values and bynk-symbol values.
	 * </p>
	 * 
	 * @param banks
	 *            - all bank names used for this mapping
	 * @param symbols
	 *            - all symbol values for this mapping
	 */
	public Mapper(String[] banks, String[] symbols) {
		if ((banks.length * symbols.length) > Byte.MAX_VALUE)
			throw new IllegalArgumentException("Number of banks*symbols should be smaller than " + Byte.MAX_VALUE);

		mapValues(banks, symbols);
	}

	/**
	 * Create the mappings. Assign byte values starting from the byte 0x0.
	 * 
	 * @param banks
	 *            - bank values
	 * @param symbols
	 *            - symbol values
	 */
	private void mapValues(String[] banks, String[] symbols) {
		mappings = new HashMap<Byte, String[]>();

		byte index = 0x0;
		for (String bank : banks) {
			for (String symbol : symbols) {
				mappings.put(index, new String[] { bank, symbol });
				index = (byte) (index + 1);
			}
		}
	}

	/**
	 * Get the bank value from the identifier.
	 * 
	 * @param identifier
	 *            - byte identifier
	 * @return bank value
	 */
	public String getBank(byte identifier) {
		return mappings.get(identifier)[0];
	}

	/**
	 * Get symbol value from the identifier.
	 * 
	 * @param identifier
	 *            - byte identifier
	 * @return symbol value
	 */
	public String getSymbol(byte identifier) {
		return mappings.get(identifier)[1];
	}

	/**
	 * Return identifier value depending on bank-symbol combination.
	 * 
	 * @param bank
	 *            - bank value
	 * @param symbol
	 *            - symbol value
	 * @return identifier
	 */
	public byte getIdentifier(String bank, String symbol) {
		Set<Byte> keys = mappings.keySet();
		for (Byte key : keys) {
			if (Arrays.equals(mappings.get(key), new String[] { bank, symbol }))
				return key;
		}

		throw new IllegalArgumentException("No valid bank-symbol pair provided");
	}
}
