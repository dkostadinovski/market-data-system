package com._360t.marketdatasystem.transferable.marketdata;

import java.math.BigDecimal;

/**
 * This class encloses byte identifier which maps to bank-symbol value.
 * 
 * @see MarketData
 * @see MarketDataBasic
 * @see Mapper
 *
 */
public class MarketDataOptimized extends MarketData {
	private static final long serialVersionUID = 1L;

	// Parameters
	private byte identifier;
	private BigDecimal price;

	/**
	 * Constructor.
	 * 
	 * @param ID
	 *            - identification of this object
	 * @param identifier
	 *            - provides info about the bank and symbol value
	 * @param price
	 *            - the price for this market data
	 */
	public MarketDataOptimized(int ID, byte identifier, BigDecimal price) {
		super(ID);
		this.identifier = identifier;
		this.price = price;
	}

	/**
	 * Return bank-symbol identifier.
	 * 
	 * @return bank-symbol identifier
	 */
	public byte getIdentifier() {
		return identifier;
	}

	/**
	 * Get bank value.
	 * 
	 * @param mapper
	 *            - contains bank-identifier mappings
	 * @return bank value
	 */
	public String getBank(Mapper mapper) {
		return mapper.getBank(identifier);
	}

	/**
	 * Get symbol value.
	 * 
	 * @param mapper
	 *            - contains symbol-identifier mappings
	 * @return symbol value
	 */
	public String getSymbol(Mapper mapper) {
		return mapper.getSymbol(identifier);
	}

	/**
	 * Get price of this market data object.
	 * 
	 * @return price value
	 */
	public BigDecimal getPrice() {
		return price;
	}
}
