package com._360t.marketdatasystem.transferable.marketdata;

import com._360t.marketdatasystem.transferable.Transferable;

/***
 * This class encloses market data info. It contains info about MarketData's ID.
 * 
 * @see Transferable
 *
 */
public abstract class MarketData extends Transferable {
	private static final long serialVersionUID = 1L;

	// Market data ID
	private int ID;

	/**
	 * Default constructor.
	 * 
	 * @param ID
	 *            - market data identification
	 */
	public MarketData(int ID) {
		this.ID = ID;
	}

	/**
	 * Return Market Data ID.
	 * 
	 * @return ID of this MarketData object
	 */
	public int getID() {
		return ID;
	}
}
