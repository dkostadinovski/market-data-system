package com._360t.marketdatasystem.transferable.marketdata;

import java.math.BigDecimal;

/**
 * This class encloses market data info and contains actual values of objects
 * {@code bank} and {@code symbol}.
 * 
 * @see MarketData
 * @see MarketDataOptimized
 *
 */
public class MarketDataBasic extends MarketData {
	private static final long serialVersionUID = 1L;

	// Parameters
	private String symbol;
	private String bank;
	private BigDecimal price;

	/**
	 * Constructor.
	 * 
	 * @param ID
	 *            - Market Data identification
	 * @param symbol
	 *            - currency symbol
	 * @param bank
	 *            - the bank providing market data info
	 * @param price
	 *            - update in price
	 */
	public MarketDataBasic(int ID, String symbol, String bank, BigDecimal price) {
		super(ID);
		this.symbol = symbol;
		this.bank = bank;
		this.price = price;
	}

	/**
	 * Get currency.
	 * 
	 * @return currency symbol
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * Get bank's name.
	 * 
	 * @return the name of the bank
	 */
	public String getBank() {
		return bank;
	}

	/**
	 * Get price.
	 * 
	 * @return price value
	 */
	public BigDecimal getPrice() {
		return price;
	}
}
