package com._360t.marketdatasystem.transferable;

import java.io.Serializable;

/**
 * This class should be extended by objects which are part of some transfer of
 * data. It can track the starting and finishing time of the transmission.
 * 
 */
public class Transferable implements Serializable {
	private static final long serialVersionUID = 1L;

	// End and start time
	private long startTransferTime;
	private long endTransferTime;

	/**
	 * This enumeration indicates the transfer period.
	 */
	public enum TransferPeriod {
		START, END
	}

	/**
	 * Get the time when the transmission of this object began.
	 * 
	 * @return transmission start time
	 */
	public long getStartTransferTime() {
		return startTransferTime;
	}

	/**
	 * Get the time when the transmission of this object ended.
	 * 
	 * @return transmission end time
	 */
	public long getEndTransferTime() {
		return endTransferTime;
	}

	/**
	 * Track transmission time depending on transmission period.
	 * 
	 * @param transferType
	 *            - {@link TransferPeriod}
	 */
	public void setTransferTime(TransferPeriod transferType) {
		long currTime = System.currentTimeMillis();

		switch (transferType) {
		case START:
			startTransferTime = currTime;
			break;
		case END:
			endTransferTime = currTime;
			break;
		default:
			System.err.println("Not a valid TransferPeriod provided.");
			break;
		}
	}
}
