:: Windows-Script
:: This script is used to start Jetty server and run 
:: simulation for uploading market data objects
 
:: Compile code and create JAR 
call mvn clean compile assembly:single

SETLOCAL
:: Configuration Properties Path
Set _config_path=config.properties

cd target
start "SERVER" java -cp market-data-system-0.1-SNAPSHOT-jar-with-dependencies.jar com._360t.marketdatasystem.server.Server360t %_config_path%

start "Simulator" java -cp market-data-system-0.1-SNAPSHOT-jar-with-dependencies.jar com._360t.marketdatasystem.Simulator %_config_path%

