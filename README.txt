
Task description


Having a market data structure containing at least the fields:

String symbol; //for example "EUR/USD"
String bank;   //source bank who published the price
BigDecimal price; //current price update

Develop a Client/Server system for uploading market data updates to a remote location.

The Server application should offer as connectivity endpoint an embeded Jetty servlet engine (http://www.eclipse.org/jetty/) with necessary servlet developed by you.

The Client application should:

Upload a stream of 10000 simulated market data objects to server.
Open a download stream to server and retrieve for every uploaded object statistics informations (statistics should contain at least the time when the uploaded data reached the server).
Measure the round-trip time of every market data: round trip time = upload time + statistic download time.
Save the measured values to a file (comma separated values).
The marked data objects should be uploaded immediately after they are created one by one (do not wait for all 10000 objects to be created).
Avoid creating a request for every market data object.

Environment requirements:

1. Develop the solution in Java 7 using maven as build tool and jetty version 8.
2. Provide script or command line for starting the server and the client application.

Additional challenges (not required)

1: advance a proposal for communication optimization knowing that there are only 4 possible banks and 5 possible symbols.

2: measure the transfered data (total bytes up, total bytes down).

3: implement the proposal (additional challenges point 1) and compare with the first version.

4: use https instead of http



======================================================================
This project demonstrates the exchange of MarketData objects in client-server system.
Configuration file is provided in /src/main/resources/config.properties.

A Batch script is provided for running the server and simulation example:
 - start-cmd.bat (Windows)
3 Bash scripts are provided for running the application on Unix system:
 - 1-bash-mvn-compile.sh; 2-bash-start-server.sh; 3-bash-start-client.sh


Transferable (data exchanged)
    |   
    |-MarketData (ID)
    |     |
    |	  |- MarketDataBasic (Bank, Symbol, Price)
    | 	  |- MarketDataOptimized (Identifier, Price); aditional tasks 1 and 3
    |	  |- MarketDataStatistics (info about data upload/download)
    |
    |-Mapper (maps values between identifier and bank-symbol; addition tasks 1 and 3;



Additional challenges:
1. Advance a proposal for communication optimization knowing that there are only 4 possible banks and 5 possible symbols.
 - Since we know that there are only 4 banks and 5 symbols, we can calculate all possible combinations of bank-symbol values (4*5=20 in our case). All combinations can be saved in one Byte (up to 256 combinations), so I propose sending one-byte value identifier for bank-symbol value instead of writing banks' and symbols' full names.

2. Measure the transfered data (total bytes up, total bytes down)
 - 'ObjectSize' class returns any object's size in bytes. Size of every sent/received object is added to overall transfered data.

3. Implement the proposal (additional challenges point 1) and compare with the first version.
 - MarketDataOptimized is subclassed from MarketData class. 
 - MarketDataOptimized contains byte-identifier for bank-symbol value.
 - When client sends stream of MarketDataOptimized objects, at the beggining of the stream it should also provide/send object of type 'Mapper'.
 - 'Mapper' contains all combinations of bank-symbol mappings to byte-identifiers sent in the current stream, so the receiving side can easily get values 'bank' and 'symbol'
 - Test:
   - MarketDataBasic: 10000 objects; Uploaded traffic: 6788228 Bytes;
   - MarektDataOptimized: 10000 objects + 1 Mapper; Uploaded traffic: 6350278 Bytes;
   - Difference: 437950 Bytes
 - It can be better optimized if Server mantains mappings itself, so no need of sending Mapper instance in every stream.

4. Use https instead of http
 - Servlet is available through both HTTP and HTTPS protocol (by default on ports 8080 and 8443).
 - For HTTPS self-signed server's certificate is used. Certificate has CommonName=localhost, so HTTPs protocol can be used only on localhost address.