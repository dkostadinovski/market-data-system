#!/bin/bash
# Unix-Script
# Run Client Simulator

# Configuration Properties Path
config_path=config.properties
cd target

java -cp market-data-system-0.1-SNAPSHOT-jar-with-dependencies.jar com._360t.marketdatasystem.Simulator $config_path 
