
Task description

Having a market data structure containing at least the fields:

String symbol; //for example "EUR/USD"
String bank;   //source bank who published the price
BigDecimal price; //current price update

Develop a Client/Server system for uploading market data updates to a remote location.

The Server application should offer as connectivity endpoint an embeded Jetty servlet engine (http://www.eclipse.org/jetty/) with necessary servlet developed by you.

The Client application should:

Upload a stream of 10000 simulated market data objects to server.
Open a download stream to server and retrieve for every uploaded object statistics informations (statistics should contain at least the time when the uploaded data reached the server).
Measure the round-trip time of every market data: round trip time = upload time + statistic download time.
Save the measured values to a file (comma separated values).
The marked data objects should be uploaded immediately after they are created one by one (do not wait for all 10000 objects to be created).
Avoid creating a request for every market data object.

Environment requirements:

1. Develop the solution in Java 7 using maven as build tool and jetty version 8.
2. Provide script or command line for starting the server and the client application.

Additional challenges (not required)

1: advance a proposal for communication optimization knowing that there are only 4 possible banks and 5 possible symbols.

2: measure the transfered data (total bytes up, total bytes down).

3: implement the proposal (additional challenges point 1) and compare with the first version.

4: use https instead of http

